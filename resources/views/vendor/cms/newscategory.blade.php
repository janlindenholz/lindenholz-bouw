@extends(config('cms.base_layout'))

@section('title', $category->title)
@include('cms::frontend.news.base')

@section('content')

<h1>{{ $category->title }}</h1>

@foreach ($category->items as $item)
    @include('cms::frontend.news.item')
@endforeach

@stop
