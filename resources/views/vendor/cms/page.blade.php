@extends(config('cms.base_layout'))

@section('title', $page->title)

@section('content')

<h1 class="title">{{ $page->title }}</h1>

<div class="text">{!! $page->text !!}</div>

@if($page->contactform)
	@include('cms::frontend.partials.contactForm')
@endif
@stop