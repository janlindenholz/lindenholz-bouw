<h3><strong>Contact</strong>formulier</h3>
{{ Form::open(array('route' => array('frontend.contactform', '1'), "id"=>"contactForm", "class"=>"form-horizontal")) }}
    <div class="form-group">
		{{ Form::label("name", "Naam: ", array("class"=>"col-sm-3 control-label")) }}	
		<div class="col-sm-7">
			{{ Form::text("name", null, array("placeholder"=>"Vul uw volledige naam in.", "class"=>"form-control"))}}
		</div>
	</div>
        
    <div class="form-group">
		{{ Form::label("email", "E-mail adres: ", array("class"=>"col-sm-3 control-label")) }}	
		<div class="col-sm-7">
			{{ Form::email("email", null, array("placeholder"=>"Vul uw e-mail adres in.", "class"=>"form-control"))}}
		</div>
	</div>

	<div class="form-group">
		<label for="emailCheck" class="col-sm-3 control-label emailCheck">E-mail adres:<br><small>ter controle</small></label>
		<div class="col-sm-7">
			{{ Form::email("email_confirmation", null, array("placeholder"=>"Vul nogmaals uw e-mail adres in.", "class"=>"form-control", "id"=>"emailCheck"))}}
		</div>
	</div>

	<div class="form-group">
		{{ Form::label("phone", "Telefoon: ", array("class"=>"col-sm-3 control-label")) }}	
		<div class="col-sm-7">
			{{ Form::text("phone", null, array("placeholder"=>"Telefoonnummer", "class"=>"form-control", "id"=>"phone")) }}
		</div>
	</div>

	<div class="form-group">
		{{ Form::label("message", "Opmerking: ", array("class"=>"col-sm-3 control-label")) }}	
		<div class="col-sm-7">
			{{ Form::textarea("message", null, array("placeholder"=>"Vraag of opmerking", "class"=>"form-control", "id"=>"message")) }}
		</div>
	</div>

	<div class="form-group">
        <div class="col-sm-offset-3 col-sm-7">
          {{ Form::submit("Verstuur", array("class"=>"btn btn-default")) }}
        </div>
    </div>
 {{ Form::close() }}