@extends(config('cms.base_layout'))

@section('title', 'Nieuws')
@include('cms::frontend.news.base')

@section('content')
<h1>Nieuws</h1>
@foreach ($news as $item)
    @include('cms::frontend.news.item', $item)
@endforeach

@stop
