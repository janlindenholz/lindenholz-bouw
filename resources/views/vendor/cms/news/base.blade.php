@push('css')
<link rel="stylesheet" href="{{ URL::asset('css/news.css')}}" />
@endpush

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.dotdotdot/1.7.4/jquery.dotdotdot.min.js"></script>
<script type="text/javascript">
$(function() {
    $(".newsitem .text").dotdotdot({
        after: "a.readmore"
    });
});
</script>
@endpush
