<div class="newsitem">
    <h2 class="title">
        {{ $item->title }}
        <small class="pull-right">{{ $item->created_at }}</small>
    </h2>
    <div class="label label-default">{{ $item->category->title }}</div>

    <div class="text">
        {!! $item->text !!}
        <a href="{{ route('frontend.newsitem', ['category' => $item->category->slug, 'item' => $item]) }}" class="readmore">Lees verder</a>
    </div>
</div>
