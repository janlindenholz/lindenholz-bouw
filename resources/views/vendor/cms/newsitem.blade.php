@extends(config('cms.base_layout'))

@section('title', $item->title)

@section('content')

<h1 class="title">
    {{ $item->title }}
    <small class="pull-right">{{ $item->created_at }}</small>
</h1>
<div class="label label-default">{{ $item->category->title }}</div>

<div class="text">{!! $item->text !!}</div>

@stop
